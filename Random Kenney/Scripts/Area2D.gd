extends Area2D

var dest = Global.stage[0]

func randomize_stage():
	Global.random_number()
	dest = Global.stage[Global.num/3]
# warning-ignore:return_value_discarded
	get_tree().change_scene(str("res://Scenes/" + dest + ".tscn"))


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
func _on_Goal_body_entered(body):
	Global.set_score(Global.get_score()+1)
	if Global.get_score() == 5:
		get_tree().change_scene(str("res://Scenes/Win Screen.tscn"))
	else:
		randomize_stage()# Replace with function body.


func _on_Death_body_entered(body):
	if body.get_name() == "Player":
		Global.set_lives(Global.get_lives()-1)
		if Global.get_lives() == 0:
			get_tree().change_scene(str("res://Scenes/Game Over.tscn"))
		else:
			randomize_stage()# Replace with function body.


func _on_Tutorial_body_entered(body):
	get_tree().change_scene(str("res://Scenes/Main Menu.tscn"))
	

func _on_Hit_body_entered(body):
	if body.get_name() == "Player":
		Global.set_lives(Global.get_lives()-1)
		if Global.get_lives() == 0:
			get_tree().change_scene(str("res://Scenes/Game Over.tscn"))
		else:
			randomize_stage()# Replace with function body.
