extends KinematicBody2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var animation
var velocity = Vector2(0,0)
export (int) var GRAVITY = 1200

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _on_VisibilityNotifier2D_screen_entered():
	animation = "walk"
	velocity = Vector2(-200,0)
	$AnimatedSprite.play(animation)

func _physics_process(delta):
	velocity.y += delta * GRAVITY
	velocity = move_and_slide(velocity, Vector2(0,-1))
