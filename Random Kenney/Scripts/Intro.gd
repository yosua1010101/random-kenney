extends Control


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	$Label.text = "EVERY TIME\nYOU HIT"
	yield(get_tree().create_timer(1.0),"timeout")
	$Label.text = "EVERY TIME\nYOU FALL"
	yield(get_tree().create_timer(1.0),"timeout")
	$Label.text = "EVERY TIME\nYOU GOAL"
	yield(get_tree().create_timer(1.0),"timeout")
	$Label.text = "THE STAGE\nMAY CHANGE"
	yield(get_tree().create_timer(1.0),"timeout")
	$Label.text = "SO DO YOU..."
	yield(get_tree().create_timer(1.0),"timeout")
	$Label.text = "GOOD LUCK"
	yield(get_tree().create_timer(1.0),"timeout")
	Global.set_lives(3)
	Global.random_number()
	var scene = Global.stage[Global.num / 3]
	get_tree().change_scene(str("res://Scenes/"+scene+".tscn"))

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
