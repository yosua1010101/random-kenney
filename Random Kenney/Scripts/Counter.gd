extends Label

func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	self.text = "Lives = %s\nScore = %s" % [Global.get_lives(), Global.get_score()]
