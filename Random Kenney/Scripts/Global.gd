extends Node

var rng = RandomNumberGenerator.new()
var lives = 3 setget set_lives, get_lives
var score = 0 setget set_score, get_score
var num = 0
var stage = ["Stage 1", "Stage 2", "Stage 3", "Stage 4"]
var chara = [preload("res://Character Sprite Frames/alienBlue.tres"),
			 preload("res://Character Sprite Frames/alienGreen.tres"),
			 preload("res://Character Sprite Frames/alienRed.tres")]

func set_lives(x):
	lives = x
func get_lives():
	return lives

func random_number():
	randomize()
	num = rng.randi_range(0,11)

func set_score(x):
	score = x
func get_score():
	return score
