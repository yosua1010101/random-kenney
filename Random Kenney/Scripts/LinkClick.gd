extends LinkButton


func _on_LinkButton_pressed():
	get_tree().change_scene("res://Scenes/Intro.tscn")

func _on_Menu_Link_pressed():
	get_tree().change_scene("res://Scenes/Main Menu.tscn")

func _on_LinkButton2_pressed():
	get_tree().change_scene("res://Scenes/Tutorial.tscn")
