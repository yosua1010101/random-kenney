extends KinematicBody2D

var initial_speed = 400
export (int) var speed = initial_speed
export (int) var GRAVITY = 1200
export (int) var jump_speed = -800

const UP = Vector2(0,-1)

var velocity = Vector2()

func get_input():
	var animation = "stand"
  
	velocity.x = 0
	if is_on_floor() and Input.is_action_just_pressed("ui_up"):
		velocity.y = jump_speed
	if not is_on_floor():
		animation = "jump"
	if Input.is_action_pressed('ui_right'):
		velocity.x += speed
		if is_on_floor():
			animation = "walk"
	if Input.is_action_pressed('ui_left'):
		velocity.x -= speed
		if is_on_floor():
			animation = "walk"
	$AnimatedSprite.flip_h = false if velocity.x >= 0 else true
	if $AnimatedSprite.animation != animation:
		$AnimatedSprite.play(animation)


func _physics_process(delta):
	velocity.y += delta * GRAVITY
	get_input()
	velocity = move_and_slide(velocity, UP)

func _ready():
	$AnimatedSprite.frames = Global.chara[Global.num % 3]


func _on_Area2D_body_entered(body):
	pass # Replace with function body.
